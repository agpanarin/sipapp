var kRemoteUri = "sip:agpanarin_test_2@sipnet.ru";

var kServiceName = "sipsrv";
var kRemotePortName = "SIPSRV_MESSAGE_PORT";

var gRemotePort;

function ensureServiceIsRunning(name, successCallback, errorCallback) {
	function onSuccess(contexts) {
		for (var c of contexts) {
			console.debug("  * " + c.appId);
			if (c.appId === name) {
				console.log("service found: " + name);
				successCallback();
				return;
			}
		}
		
		console.log("start service: " + name);
		tizen.application.launch(name, successCallback, errorCallback);
	}
	
	function onError() {
		console.error("cannot get app contexts");
		errorCallback();
	}
	
	console.log("get apps context");
	tizen.application.getAppsContext(onSuccess, onError);
}

function initMessagePorts() {
	function onSuccess() {
		console.log("init remote port");
		gRemotePort = tizen.messageport.requestRemoteMessagePort(serviceId, kRemotePortName);
	}
	
	function onError() {
		console.error("cannot init message ports");
	}
	
	console.log("init message ports");
	var packageInfo = tizen.package.getPackageInfo(null);
	var currentPackageId = packageInfo.id;
	var serviceId = currentPackageId + "." + kServiceName;
	ensureServiceIsRunning(serviceId, onSuccess, onError);
}

function startCall() {
	console.log("start call to: " + kRemoteUri);
	if (!gRemotePort) {
		console.error("message port is not initizlized");
		return;
	}
	gRemotePort.sendMessage([
		{ key: 'command', value: 'start_call'},
		{ key: 'uri', value: kRemoteUri},
		], null);
}

(function () {
	window.addEventListener("tizenhwkey", function (ev) {
		var activePopup = null,
			page = null,
			pageid = "";

		if (ev.keyName === "back") {
			activePopup = document.querySelector(".ui-popup-active");
			page = document.getElementsByClassName("ui-page-active")[0];
			pageid = page ? page.id : "";

			if (pageid === "main" && !activePopup) {
				try {
					tizen.application.getCurrentApplication().exit();
				} catch (ignore) {
				}
			} else {
				window.history.back();
			}
		}
	});
		
	initMessagePorts();
}());
