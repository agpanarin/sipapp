//
// Service's callback wrappers and main function.
//

// C, C++ libraries:
// System libraries:
#include <service_app.h>
// Other libraries:
// Project's headers:
#include <logging/logging.h>
#include "application.h"

bool applicationCreate(void* data) {
	try {
		logging::debug(kLogMain, "create application");
		Application **self_addr = static_cast<Application**>(data);
		*self_addr = new Application();
		return true;
	} catch (...) {
		logging::exception(kLogMain, "cannot create application");
		return false;
	}
}

void applicationTerminate(void* data) {
	try {
		logging::debug(kLogMain, "terminate application");
		Application *self = *static_cast<Application**>(data);
		delete self;
	} catch (...) {
		logging::exception(kLogMain, "cannot terminate application");
	}
}

void applicationControl(app_control_h app_control, void* data) {
	try {
		logging::debug(kLogMain, "control application");
		Application *self = *static_cast<Application**>(data);
		self->control(app_control);
	} catch (...) {
		logging::exception(kLogMain, "cannot control application");
	}
}

int main(int argc, char* argv[]) {
	logging::debug(kLogMain, "service started");

	service_app_lifecycle_callback_s callbacks;
	callbacks.create = applicationCreate;
	callbacks.terminate = applicationTerminate;
	callbacks.app_control = applicationControl;

	Application *self = nullptr;
	int ret = service_app_main(argc, argv, &callbacks, &self);
	if (ret != APP_ERROR_NONE) {
		logging::error(kLogMain, "service_app_main() failed: %d", ret);
	}

	logging::debug(kLogMain, "service stopped");
	return ret;
}
