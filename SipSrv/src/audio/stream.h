#ifndef SIPSRV__AUDIO__STREAM_H_
#define SIPSRV__AUDIO__STREAM_H_

// C, C++ libraries:
// System libraries:
#include <audio_io.h>
// Other libraries:
#include <pjmedia-audiodev/audiodev_imp.h>
// Project's headers:
#include "factory.h"

namespace audio {

class Stream {
public:
	Stream(Factory& factory,
			const pjmedia_aud_param& param,
			pjmedia_aud_rec_cb rec_cb,
			pjmedia_aud_play_cb play_cb,
			void *user_data);
	~Stream();

	pj_status_t start();
	pj_status_t stop();
	pj_status_t getParam(pjmedia_aud_param *pi);
	pj_status_t getCap(pjmedia_aud_dev_cap cap, void *pval);
	pj_status_t setCap(pjmedia_aud_dev_cap cap, const void *value);

private:
	static void audioInCallback(audio_in_h handle, size_t nbytes, void *userdata);
	static void audioOutCallback(audio_out_h handle, size_t nbytes, void *userdata);

	Factory& factory_;
	void *userData_;
	pjmedia_aud_param param_;
	pjmedia_aud_play_cb recCb_;
	char *recBuf_;
	int recBytes_;
	audio_in_h hIn_;
	pjmedia_aud_play_cb playCb_;
	int16_t *playBuf_;
	audio_out_h hOut_;
};

}  // namespace audio

#endif  // SIPSRV__AUDIO__STREAM_H_
