#ifndef SIPSRV__AUDIO___FACTORY_H_
#define SIPSRV__AUDIO___FACTORY_H_

// C, C++ libraries:
// System libraries:
// Other libraries:
#include <pjmedia/audiodev.h>
// Project's headers:

namespace audio {

class Factory {
public:
	Factory();
	void refresh();
	int getDevCount();
	pj_status_t getDevInfo(unsigned index, pjmedia_aud_dev_info *info);
	pj_status_t defaultParam(unsigned index, pjmedia_aud_param *param);
};

}  // namespace audio

#endif  // SIPSRV__AUDIO___FACTORY_H_
