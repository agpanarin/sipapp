// Main header:
#include "audio.h"
// C, C++ libraries:
#include <memory>
// System libraries:
#include <audio_io.h>
// Other libraries:
#include <pjmedia/audiodev.h>
#include <pjmedia-audiodev/audiodev_imp.h>
// Project's headers:
#include <logging/logging.h>
#include "factory.h"
#include "stream.h"

namespace audio {

void log_aud_param(const char* msg, const pjmedia_aud_param& param) {
	logging::debug(kLogPjsipAudio, "pjmedia_aud_param (%s):", msg);
	// pjmedia_dir dir;
	logging::debug(kLogPjsipAudio, "    rec_id: %i", param.rec_id);  // pjmedia_aud_dev_index rec_id;
	logging::debug(kLogPjsipAudio, "    play_id: %i", param.play_id);  // pjmedia_aud_dev_index play_id;
	logging::debug(kLogPjsipAudio, "    clock_rate: %i", param.clock_rate);  // unsigned clock_rate;
	logging::debug(kLogPjsipAudio, "    channel_count: %i", param.channel_count);  // unsigned channel_count;
	logging::debug(kLogPjsipAudio, "    samples_per_frame: %i", param.samples_per_frame);  // unsigned samples_per_frame;
	logging::debug(kLogPjsipAudio, "    bits_per_sample: %i", param.bits_per_sample);  // unsigned bits_per_sample;
	// unsigned flags;
	// pjmedia_format ext_fmt;
	// unsigned input_latency_ms;
	// unsigned output_latency_ms;
	// unsigned input_vol;
	// unsigned output_vol;
	// pjmedia_aud_dev_route input_route;
	// pjmedia_aud_dev_route output_route;
	// pj_bool_t ec_enabled;
	// unsigned ec_tail_ms;
	// pj_bool_t plc_enabled;
	// pj_bool_t cng_enabled;
	// pj_bool_t vad_enabled;
}

struct tizen_factory {
    pjmedia_aud_dev_factory	 base;
    Factory* factory;
};

static pj_status_t tizen_factory_init(pjmedia_aud_dev_factory *f)
{
	logging::debug(kLogPjsipAudio, "tizen_factory_init");
	auto pj_factory = reinterpret_cast<tizen_factory*>(f);
	pj_factory->factory = new Factory();
	return PJ_SUCCESS;
}

static pj_status_t tizen_factory_destroy(pjmedia_aud_dev_factory *f) {
	logging::debug(kLogPjsipAudio, "tizen_factory_destroy");
	auto pj_factory = reinterpret_cast<tizen_factory*>(f);
	delete pj_factory->factory;
	// TODO: delete pj_factory?
	return PJ_SUCCESS;
}

static pj_status_t tizen_factory_refresh(pjmedia_aud_dev_factory *f) {
	logging::debug(kLogPjsipAudio, "tizen_factory_refresh");
	auto pj_factory = reinterpret_cast<tizen_factory*>(f);
	pj_factory->factory->refresh();
	return PJ_SUCCESS;
}

static unsigned tizen_factory_get_dev_count(pjmedia_aud_dev_factory *f) {
	logging::debug(kLogPjsipAudio, "tizen_factory_get_dev_count");
	auto pj_factory = reinterpret_cast<tizen_factory*>(f);
	pj_factory->factory->getDevCount();
	return 1;
}

static pj_status_t tizen_factory_get_dev_info(
		pjmedia_aud_dev_factory *f,
		unsigned index,
		pjmedia_aud_dev_info *info) {
	logging::debug(kLogPjsipAudio, "tizen_factory_get_dev_info");
	auto pj_factory = reinterpret_cast<tizen_factory*>(f);
	return pj_factory->factory->getDevInfo(index, info);
}

static pj_status_t tizen_factory_default_param(
		pjmedia_aud_dev_factory *f,
		unsigned index,
		pjmedia_aud_param *param) {
	logging::debug(kLogPjsipAudio, "tizen_factory_default_param");
	auto pj_factory = reinterpret_cast<tizen_factory*>(f);
	pj_status_t res = pj_factory->factory->defaultParam(index, param);
	log_aud_param("default", *param);
	return res;
}

static pj_status_t tizen_factory_create_stream(
		pjmedia_aud_dev_factory *f,
		const pjmedia_aud_param *param,
		pjmedia_aud_rec_cb rec_cb,
		pjmedia_aud_play_cb play_cb,
		void *user_data,
		pjmedia_aud_stream **p_strm);

pjmedia_aud_dev_factory_op tizen_factory_op = {
    &tizen_factory_init,
    &tizen_factory_destroy,
    &tizen_factory_get_dev_count,
    &tizen_factory_get_dev_info,
    &tizen_factory_default_param,
    &tizen_factory_create_stream,
    &tizen_factory_refresh
};

struct tizen_aud_stream {
	pjmedia_aud_stream base;
	Stream *stream;
};

static pj_status_t tizen_stream_get_param(pjmedia_aud_stream *s, pjmedia_aud_param *pi) {
	logging::debug(kLogPjsipAudio, "tizen_stream_get_param");
	auto aud_stream = reinterpret_cast<tizen_aud_stream*>(s);
	pj_status_t res = aud_stream->stream->getParam(pi);
	log_aud_param("get stream param", *pi);
	return res;
}

static pj_status_t tizen_stream_get_cap(
		pjmedia_aud_stream *s,
		pjmedia_aud_dev_cap cap,
		void *pval) {
	logging::debug(kLogPjsipAudio, "tizen_stream_get_cap");
	auto aud_stream = reinterpret_cast<tizen_aud_stream*>(s);
	return aud_stream->stream->getCap(cap, pval);
}

static pj_status_t tizen_stream_set_cap(
		pjmedia_aud_stream *s,
		pjmedia_aud_dev_cap cap,
		const void *value) {
	logging::debug(kLogPjsipAudio, "tizen_stream_set_cap");
	auto aud_stream = reinterpret_cast<tizen_aud_stream*>(s);
	return aud_stream->stream->setCap(cap, value);
}

static pj_status_t tizen_stream_start (pjmedia_aud_stream *s) {
	logging::debug(kLogPjsipAudio, "tizen_stream_start");
	auto aud_stream = reinterpret_cast<tizen_aud_stream*>(s);
	return aud_stream->stream->start();
}

static pj_status_t tizen_stream_stop (pjmedia_aud_stream *s) {
	logging::debug(kLogPjsipAudio, "tizen_stream_stop");
	auto aud_stream = reinterpret_cast<tizen_aud_stream*>(s);
	return aud_stream->stream->stop();
}

static pj_status_t tizen_stream_destroy (pjmedia_aud_stream *s) {
	logging::debug(kLogPjsipAudio, "tizen_stream_destroy");
	auto aud_stream = reinterpret_cast<tizen_aud_stream*>(s);

	delete aud_stream->stream;
	delete aud_stream;

	return PJ_SUCCESS;
}

pjmedia_aud_stream_op tizen_stream_op = {
    &tizen_stream_get_param,
    &tizen_stream_get_cap,
    &tizen_stream_set_cap,
    &tizen_stream_start,
    &tizen_stream_stop,
    &tizen_stream_destroy
};

static pj_status_t tizen_factory_create_stream(
		pjmedia_aud_dev_factory *f,
		const pjmedia_aud_param *param,
		pjmedia_aud_rec_cb rec_cb,
		pjmedia_aud_play_cb play_cb,
		void *user_data,
		pjmedia_aud_stream **p_strm) {
	logging::debug(kLogPjsipAudio, "tizen_factory_create_stream");
	auto pj_factory = reinterpret_cast<tizen_factory*>(f);

	log_aud_param("stream", *param);
	auto aud_stream = new tizen_aud_stream();
	aud_stream->stream = new Stream(*pj_factory->factory, *param, rec_cb, play_cb, user_data);
	aud_stream->base.op = &tizen_stream_op;

	*p_strm = &aud_stream->base;
	return PJ_SUCCESS;
}

static pjmedia_aud_dev_factory* pjmedia_tizen_factory(pj_pool_factory *pf) {
	logging::debug(kLogPjsipAudio, "pjmedia_tizen_factory");

    tizen_factory *pj_factory = new tizen_factory();
    pj_factory->base.op = &tizen_factory_op;

    return &pj_factory->base;
}

bool init() {
	logging::debug(kLogPjsipAudio, "initialize tizen audio for pjsip");

	pjmedia_aud_subsys *aud_subsys = pjmedia_get_aud_subsys();
	if (!aud_subsys) {
		logging::error(kLogPjsipAudio, "cannot get audio subsystem");
		return false;
	}

	pj_status_t res = pjmedia_aud_register_factory(pjmedia_tizen_factory);
	if (res != PJ_SUCCESS) {
		logging::error(kLogPjsipAudio, "cannot register audio factory");
		return false;
	}

	return true;
}

}  // namespace audio
