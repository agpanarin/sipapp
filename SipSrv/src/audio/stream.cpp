// Main header:
#include "stream.h"
// C, C++ libraries:
// System libraries:
// Other libraries:
#include <pjsua2/endpoint.hpp>
// Project's headers:
#include <logging/logging.h>

namespace audio {

Stream::Stream(Factory& factory,
		const pjmedia_aud_param& param,
		pjmedia_aud_rec_cb rec_cb,
		pjmedia_aud_play_cb play_cb,
		void* user_data) :
			factory_(factory),
			userData_(user_data),
			param_(param),
			recCb_(rec_cb),
			recBytes_(0),
			playCb_(play_cb) {
	playBuf_ = new int16_t[param_.samples_per_frame];
	recBuf_ = new char[param_.samples_per_frame * 2];

	int err = audio_out_create_new(param_.clock_rate, AUDIO_CHANNEL_MONO, AUDIO_SAMPLE_TYPE_S16_LE, &hOut_);
	if (err != AUDIO_IO_ERROR_NONE) {
		logging::debug(kLogPjsipAudio, "cannot create audio out");
		return;  // TODO: Process error??
	}

	err = audio_out_set_stream_cb(hOut_, audioOutCallback, this);
	if (err != AUDIO_IO_ERROR_NONE) {
		logging::debug(kLogPjsipAudio, "cannot set audio out callback");
		audio_out_destroy(hOut_);
		return;
	}

	hIn_ = nullptr;

#if 1
	err = audio_in_create(param_.clock_rate, AUDIO_CHANNEL_MONO, AUDIO_SAMPLE_TYPE_S16_LE, &hIn_);
	if (err != AUDIO_IO_ERROR_NONE) {
		logging::warning(kLogPjsipAudio, "cannot create audio in: %i", err);
		return;
	}

	err = audio_in_set_stream_cb(hIn_, audioInCallback, this);
	if (err != AUDIO_IO_ERROR_NONE) {
		logging::warning(kLogPjsipAudio, "cannot set audio in callback");
		audio_in_destroy(hIn_);
		hIn_ = nullptr;
		return;
	}
#endif
}

Stream::~Stream() {
	int err;

	if (hOut_) {
		int err = audio_out_unset_stream_cb(hOut_);
		if (err != AUDIO_IO_ERROR_NONE) {
			logging::error(kLogPjsipAudio, "cannot unset audio out callback");
		}

		err = audio_out_destroy(hOut_);
		if (err != AUDIO_IO_ERROR_NONE) {
			logging::error(kLogPjsipAudio, "cannot destroy audio out");
		}
	}

	if (hIn_) {
		err = audio_in_unset_stream_cb(hIn_);
		if (err != AUDIO_IO_ERROR_NONE) {
			logging::error(kLogPjsipAudio, "cannot unset audio in callback");
		}
		err = audio_in_destroy(hIn_);
		if (err != AUDIO_IO_ERROR_NONE) {
			logging::error(kLogPjsipAudio, "cannot destroy audio in");
		}
	}

	delete[] playBuf_;
	delete[] recBuf_;
}

pj_status_t Stream::start() {
	int err;

	if (hOut_) {
		err = audio_out_prepare(hOut_);
		if (err != AUDIO_IO_ERROR_NONE) {
			logging::error(kLogPjsipAudio, "cannot prepare audio out");
			// TODO: error processing??
		}
	}

	if (hIn_) {
		err = audio_in_prepare(hIn_);
		if (err != AUDIO_IO_ERROR_NONE) {
			logging::error(kLogPjsipAudio, "cannot prepare audio in");
			// TODO: error processing??
		}
	}

	return PJ_SUCCESS;
}

pj_status_t Stream::stop() {
	int err;

	if (hOut_) {
		err = audio_out_unprepare(hOut_);
		if (err != AUDIO_IO_ERROR_NONE) {
			logging::error(kLogPjsipAudio, "cannot unprepare audio out");
			// TODO: error processing??
		}
	}

	if (hIn_) {
		err = audio_in_unprepare(hIn_);
		if (err != AUDIO_IO_ERROR_NONE) {
			logging::error(kLogPjsipAudio, "cannot unprepare audio in");
			// TODO: error processing??
		}
	}

	return PJ_SUCCESS;
}

pj_status_t Stream::getParam(pjmedia_aud_param *pi) {
	factory_.defaultParam(0, pi);
	pi->samples_per_frame = param_.samples_per_frame;
	return PJ_SUCCESS;
}

pj_status_t Stream::getCap(pjmedia_aud_dev_cap cap, void *pval) {
	return PJMEDIA_EAUD_INVCAP;
}

pj_status_t Stream::setCap(pjmedia_aud_dev_cap cap, const void *value) {
    return PJMEDIA_EAUD_INVCAP;
}

void Stream::audioInCallback(audio_in_h handle, size_t nbytes, void *userdata) {
	logging::trace(kLogPjsipAudio, "audio_in_callback start (nbytes: %i)", nbytes);
	auto stream = static_cast<Stream*>(userdata);

	if (!pj::Endpoint::instance().libIsThreadRegistered()) {
		logging::debug(kLogPjsipAudio, "register thread");
		pj::Endpoint::instance().libRegisterThread("audioInCallbackThread");  // TODO: Exception!!
	}

	unsigned int bytes = nbytes;
	const char* buf = nullptr;
	int res = audio_in_peek(handle, (const void**)&buf, &bytes);
	if (res < 0) {
		logging::error(kLogPjsipAudio, "cannot peek audio in: %i", res);
		return;
	}

	while (bytes) {
		int maxBytes = stream->param_.samples_per_frame * 2 - stream->recBytes_;
		int readBytes = (bytes <= maxBytes) ? bytes : maxBytes;
		memcpy(stream->recBuf_ + stream->recBytes_, buf, readBytes);

		buf += readBytes;
		stream->recBytes_ += readBytes;
		bytes -= readBytes;

		logging::debug(kLogPjsipAudio, "recBytes: %i", stream->recBytes_);
		if (stream->recBytes_ == stream->param_.samples_per_frame * 2) {
			stream->recBytes_ = 0;

			static pj_uint64_t timestamp = 0;

			pjmedia_frame frame;
			pj_bzero(&frame, sizeof(frame));
			frame.type = PJMEDIA_FRAME_TYPE_AUDIO;
			frame.buf = stream->recBuf_;
			frame.size = stream->param_.samples_per_frame * 2;
			frame.timestamp.u64 = timestamp;
			timestamp += stream->param_.samples_per_frame;

			stream->recCb_(stream->userData_, &frame);
		}
	}

	audio_in_drop(handle);
}

void Stream::audioOutCallback(audio_out_h handle, size_t nbytes, void *userdata) {
	logging::trace(kLogPjsipAudio, "audio_out_callback start (nbytes: %i)", nbytes);
	auto stream = static_cast<Stream*>(userdata);

	if (!pj::Endpoint::instance().libIsThreadRegistered()) {
		logging::debug(kLogPjsipAudio, "register thread");
		pj::Endpoint::instance().libRegisterThread("audioOutCallbackThread");  // TODO: Exception!!
	}

	static pj_uint64_t timestamp = 0;

	pjmedia_frame frame;
	frame.type = PJMEDIA_FRAME_TYPE_AUDIO;
	frame.buf = stream->playBuf_;
	frame.size = stream->param_.samples_per_frame * 2;
	frame.timestamp.u64 = 0;
	frame.bit_info = 0;

	for (int n = 0; n < nbytes / frame.size + 1; n++) {
		for (int i = 0; i < frame.size/2; i++) {
			int16_t* buf = static_cast<int16_t*>(frame.buf);
			buf[i] = 24000 * sin(i * 2*3.1415926 / 16);
		}

		frame.timestamp.u64 = timestamp;
		frame.timestamp.u32.hi = timestamp >> 32;
		frame.timestamp.u32.lo = timestamp;
		timestamp += frame.size / 2;

		//stream->recCb_(stream->userData_, &frame);

		//dlog_print(LOG_VERBOSE, LOG_AUDIO, "call play_cb (frame.size: %i)", frame.size);
		stream->playCb_(stream->userData_, &frame);  // TODO: Process error.

		//dlog_print(LOG_VERBOSE, LOG_AUDIO, "send data to audio dev (frame.size: %i)", frame.size);
		int size = audio_out_write(stream->hOut_, frame.buf, frame.size);

		//dlog_print(LOG_VERBOSE, LOG_AUDIO, "audio_out_write result: %i", size);
	}

	logging::trace(kLogPjsipAudio, "audio_out_callback end");
}

}  // namespace audio
