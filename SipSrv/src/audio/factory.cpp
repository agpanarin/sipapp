// Main header:
#include "factory.h"
// C, C++ libraries:
// System libraries:
// Other libraries:
// Project's headers:

namespace audio {

Factory::Factory() {
	;
}

void Factory::refresh() {
	;
}

int Factory::getDevCount() {
	return 1;
}

pj_status_t Factory::getDevInfo(unsigned index, pjmedia_aud_dev_info *info) {
	strcpy(info->name, "TizenAudioDev");
	info->input_count = 1;
	info->output_count = 1;
	info->default_samples_per_sec = 16000;
	strcpy(info->driver, "TizenAudioDrv");
	info->caps = 0;
	info->routes = 0;
	info->ext_fmt_cnt = 0;

	return PJ_SUCCESS;
}

pj_status_t Factory::defaultParam(unsigned index, pjmedia_aud_param *param) {
	pj_bzero(param, sizeof(*param));
	param->dir = PJMEDIA_DIR_CAPTURE_PLAYBACK;
	param->rec_id = 0;
	param->play_id = 0;
	param->clock_rate = 16000;
	param->channel_count = 1;
	param->samples_per_frame = 16000 / 10;
	param->bits_per_sample = 16;
	param->flags = 0;
	param->input_latency_ms = PJMEDIA_SND_DEFAULT_REC_LATENCY;
	param->output_latency_ms = PJMEDIA_SND_DEFAULT_PLAY_LATENCY;

	return PJ_SUCCESS;
}

}  // namespace audio
