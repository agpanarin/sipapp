// Main header:
#include "logging.h"
// C, C++ libraries:
// System libraries:
#include <dlog.h>
// Other libraries:
// Project's headers:

namespace logging {

#define DEFINE_LOG_FUNCTION(name, level) \
	void name(const char* tag, const char* format, ...) { \
		if (level >= kLogLevel) { \
			va_list args; \
			va_start(args, format); \
			dlog_vprint(level, tag, format, args); \
			va_end(args); \
		} \
	}

DEFINE_LOG_FUNCTION(fatal, DLOG_FATAL);
DEFINE_LOG_FUNCTION(error, DLOG_ERROR);
DEFINE_LOG_FUNCTION(warning, DLOG_WARN);
DEFINE_LOG_FUNCTION(info, DLOG_INFO);
DEFINE_LOG_FUNCTION(debug, DLOG_DEBUG);
DEFINE_LOG_FUNCTION(trace, DLOG_VERBOSE);

void exception(const char* tag, const char* msg) {
	const char* message = msg ? msg : "exception";
	try {
		throw;
	} catch (const std::exception& ex) {
		error(tag, "%s: %s", message, ex.what());
	} catch (const pj::Error& ex) {
		error(tag, "%s: %s", message, ex.info().c_str());
	} catch (...) {
		error(tag, "$s: unknown error", message);
	}
}

}  // namespace logging
