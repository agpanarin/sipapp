#ifndef SIPSRV__LOGGING__LOGGING_H_
#define SIPSRV__LOGGING__LOGGING_H_

// C, C++ libraries:
// System libraries:
#include <dlog.h>
// Other libraries:
#include <pjsua2/endpoint.hpp>
// Project's headers:

const log_priority kLogLevel = DLOG_DEBUG;

const char* const kLogRoot = "SIPSRV";
const char* const kLogMain = "SIPSRV_MAIN";
const char* const kLogApp = "SIPSRV_APP";
const char* const kLogPjsip = "SIPSRV_PJSIP";
const char* const kLogPjsipAudio = "SIPSRV_PJSIP_AUDIO";
const char* const kLogMsgport = "SIPSRV_MSGPORT";

namespace logging {

void fatal(const char* tag, const char* fmt, ...);
void error(const char* tag, const char* fmt, ...);
void warning(const char* tag, const char* fmt, ...);
void info(const char* tag, const char* fmt, ...);
void debug(const char* tag, const char* fmt, ...);
void trace(const char* tag, const char* fmt, ...);

void exception(const char* tag, const char* msg);

}  // namespace logging

class PjLogWriter : public pj::LogWriter {
	void write(const pj::LogEntry& entry) {
		// TODO: use log level from 'entry'?
		logging::debug(kLogPjsip, "%s", entry.msg.c_str());
	}
};

#endif  // SIPSRV__LOGGING__LOGGING_H_
