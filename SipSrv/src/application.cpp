// Main header:
#include "application.h"
// C, C++ libraries:
// System libraries:
#include <unistd.h>
// Other libraries:
// Project's headers:
#include <logging/logging.h>
#include <audio/audio.h>

const std::string kLocalPortName = "SIPSRV_MESSAGE_PORT";
const std::string kLocalRemoteName = "SIPAPP_MESSAGE_PORT";

Application::Application():
		localPort_(kLocalPortName, [this](bundle* msg){this->onPortMessage(msg);}) {
	logging::debug(kLogPjsip, "initialize endpoint");
	endpointConfig_.reset(new pj::EpConfig());

	endpointConfig_->logConfig.level = 10;
	endpointConfig_->logConfig.writer = new PjLogWriter();

	endpointConfig_->medConfig.noVad = true;

	endpointConfig_->uaConfig.natTypeInSdp = 2;  // add both NAT type number and name.
	endpointConfig_->uaConfig.stunServer.push_back("stun.sipnet.ru");

	endpoint_.reset(new pj::Endpoint());
	endpoint_->libCreate();
	endpoint_->libInit(*endpointConfig_);

	endpoint_->natDetectType();
	sleep(3);

	logging::debug(kLogPjsip, "create transport");
	transportConfig_.reset(new pj::TransportConfig());
	//transportConfig_->port = 5060;
	transportConfig_->port = 6050;
	//endpoint_->transportCreate(PJSIP_TRANSPORT_TCP, *transportConfig_);
	endpoint_->transportCreate(PJSIP_TRANSPORT_UDP, *transportConfig_);
	endpoint_->libStart();

	//sleep(10);

	logging::debug(kLogPjsip, "create account config");
	accountConfig_.reset(new pj::AccountConfig());
	//accountConfig_->idUri = "sip:agpanarin_test_1@sipnet.ru;transport=tcp";
	accountConfig_->idUri = "sip:agpanarin_test_1@sipnet.ru";
	accountConfig_->regConfig.registrarUri = "sip:sipnet.ru";
	accountConfig_->sipConfig.authCreds.push_back(
			pj::AuthCredInfo("digest", "*", "agpanarin_test_1", 0, "ecY-7PF-RoW-gUx"));

	//sleep(10);

	logging::debug(kLogPjsip, "create account");
	account_.reset(new MyAccount());
	account_->create(*accountConfig_);

	audio::init();

#if 0
	sleep(10);

	logging::debug(kLogPjsip, "start call");
	//std::string destUri = "sip:agpanarin_test_2@sipnet.ru;transport=tcp";  // Gvk-7EA-cLe-TQ3
	std::string destUri = "sip:agpanarin_test_2@sipnet.ru";  // Gvk-7EA-cLe-TQ3
	pj::CallOpParam prm(true);
	call_.reset(new MyCall(*account_));
	call_->makeCall(destUri, prm);
#elif 0
	logging::debug(kLogPjsip, "test playback");
	pj::AudioMediaPlayer *player = new pj::AudioMediaPlayer();
	pj::AudioMedia& play_med = pj::Endpoint::instance().audDevManager().getPlaybackDevMedia();

	player->createPlayer("/opt/usr/home/owner/apps_rw/org.example.basicui/res/test.wav");
	player->startTransmit(play_med);
#endif
}

void Application::onPortMessage(bundle* message) {
	char* command;
	bundle_get_str(message, "command", &command);
	logging::debug(kLogMsgport, "command: %s", command);

	if (!strcmp(command, "start_call")) {
		char* uri;
		bundle_get_str(message, "uri", &uri);
		logging::debug(kLogMsgport, "uri: %s", uri);

		logging::info("start call to: %s", uri);
		pj::CallOpParam prm(true);
		MyCall* call = new MyCall(*account_);
		call->makeCall(std::string(uri), prm);
	}
}
