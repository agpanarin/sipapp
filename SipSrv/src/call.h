#ifndef CALL_H_
#define CALL_H_

#include <pjsua2/call.hpp>
#include <pjsua2/account.hpp>

#include <logging/logging.h>

class MyCall : public pj::Call {
public:
	MyCall(pj::Account &acc, int call_id = PJSUA_INVALID_ID): pj::Call(acc, call_id) {}

	void onCallState(pj::OnCallStateParam &prm) override {
		pj::SipEvent e = prm.e;
		pj::SipEventBody body = e.body;
		pjsip_event_id_e type = e.type;
		logging::debug(kLogPjsip, "onCallState, type: %i", type);

		if (type == PJSIP_EVENT_TSX_STATE) {
			pj::TsxStateEvent state = body.tsxState;
			pj::SipTransaction transaction = state.tsx;
		}
	}

	void onCallMediaState(pj::OnCallMediaStateParam &prm) override {
	    pj::CallInfo ci = getInfo();

	    for (int i = 0; i < ci.media.size(); i++) {
	        if (ci.media[i].type==PJMEDIA_TYPE_AUDIO && getMedia(i)) {
	            pj::AudioMedia *aud_med = (pj::AudioMedia *)getMedia(i);

	            pj::AudDevManager& mgr = pj::Endpoint::instance().audDevManager();
	            aud_med->startTransmit(mgr.getPlaybackDevMedia());
	            mgr.getCaptureDevMedia().startTransmit(*aud_med);
	        }
	    }
	}
};

class MyAccount : public pj::Account {
public:
	MyAccount() {}
	void onRegState(pj::OnRegStateParam &param) override {}
	void onIncomingCall(pj::OnIncomingCallParam &param) override {
		logging::info(kLogApp, "incoming call...");
		auto call = new MyCall(*this, param.callId);
		pj::CallOpParam prm;
		prm.statusCode = PJSIP_SC_OK;
		call->answer(prm);
	}
};

#endif  // CALL_H_
