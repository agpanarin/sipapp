#ifndef SIPSRV__APPLICATION__APPLICATION_H_
#define SIPSRV__APPLICATION__APPLICATION_H_

// C, C++ libraries:
#include <memory>
// System libraries:
#include <service_app.h>
// Other libraries:
#include <pjsua2/endpoint.hpp>
#include <pjsua2/account.hpp>
#include <pjsua2/call.hpp>
// Project's headers:
#include <logging/logging.h>
#include <msgport/msgport.h>
#include "call.h"

class Application {
public:
	Application();

	~Application() {
		endpoint_->libDestroy();
	}

	void control(app_control_h app_control) {}

	void onPortMessage(bundle* message);

private:
	msgport::Local localPort_;
	PjLogWriter logWriter_;
	std::unique_ptr<pj::Endpoint> endpoint_;
	std::unique_ptr<pj::EpConfig> endpointConfig_;
	std::unique_ptr<pj::TransportConfig> transportConfig_;
	std::unique_ptr<pj::AccountConfig> accountConfig_;
	std::unique_ptr<MyAccount> account_;
	std::unique_ptr<MyCall> call_;
};

#endif  // SIPSRV__APPLICATION__APPLICATION_H_
