#ifndef SIPSRV__MSGPORT__MSGPORT_H_
#define SIPSRV__MSGPORT__MSGPORT_H_

// C, C++ libraries:
#include <string>
#include <functional>
// System libraries:
#include <message_port.h>
// Other libraries:
// Project's headers:

namespace msgport {

class Local {
public:
	typedef std::function<void(bundle* message)> callback_t;

	Local(const std::string& name,  callback_t callback);
	virtual ~Local();

private:
	static void onMessage(int local_port_id, const char *remote_app_id,
			const char *remote_port, bool trusted_remote_port,
			bundle *message, void *user_data);

	std::string name_;
	callback_t callback_;
	int id_;
};

}  // namespace msgport

#endif  // SIPSRV__MSGPORT__MSGPORT_H_
