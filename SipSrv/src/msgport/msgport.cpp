// Main header:
#include "msgport.h"
// C, C++ libraries:
// System libraries:
// Other libraries:
// Project's headers:
#include <logging/logging.h>

namespace msgport {

Local::Local(const std::string& name, Local::callback_t callback)
		: name_(name),
		  callback_(callback) {
	logging::debug(kLogMsgport, "register local port '%s'", name_.c_str());
	id_ = message_port_register_local_port(name_.c_str(), onMessage, this);
	if (id_ < 0) {
		// TODO: convert error to string and throw (what?)
		logging::error(kLogMsgport, "cannot register local port '%s', error: %i", name_.c_str(), id_);
	}
}

Local::~Local() {
	logging::debug(kLogMsgport, "unregister local port '%s'", name_.c_str());
	int res = message_port_unregister_local_port(id_);
	if (res < 0) {
		logging::error("cannot unregister local port '%s', error: ", name_.c_str(), res);
	}
}

void Local::onMessage(int local_port_id, const char *remote_app_id,
			const char *remote_port, bool trusted_remote_port,
			bundle *message, void *user_data) {
	logging::debug(kLogMsgport, "local port callback");
	auto self = static_cast<Local*>(user_data);
	self->callback_(message);
}

}  // namespace msgport
