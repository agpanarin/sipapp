#
# General vars and rules for build autotools-based projects.
#
# NOTE: Tizen SDK must be installed in default path: $(HOME)/tizen-studio
#

HOST := i386-linux-gnueabi
_TIZEN := $(HOME)/tizen-studio
_PLATFORM := $(_TIZEN)/platforms/tizen-3.0/mobile/rootstraps/mobile-3.0-emulator.core

PROJECT := $(shell pwd)
SOURCES := $(PROJECT)/vendor
BUILD := $(PROJECT)/_build
INSTALL := $(PROJECT)/_install

export PATH := $(PATH):$(_TIZEN)/tools/$(HOST)-gcc-4.9/bin
export CFLAGS := $(CFLAGS) --sysroot=$(_PLATFORM)
export CXXFLAGS := $(CXXFLAGS) --sysroot=$(_PLATFORM)
export CPPFLAGS := $(CPPFLAGS) --sysroot=$(_PLATFORM)
export LDFLAGS := $(LDFLAGS) --sysroot=$(_PLATFORM)

.PHONY: all clean make-builddir copy-sources remove-all
 
all:

clean: remove-all

make-builddir:
	mkdir -p $(BUILD)

copy-sources:
	cp -a $(SOURCES)/ $(BUILD)

remove-all:
	rm -rf $(PROJECT)/_build
	rm -rf $(PROJECT)/_install
